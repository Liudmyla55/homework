console.log('Homework 10_js');

const frmPass = document.getElementById('frmPass');


const lblEnter = document.getElementById('lblEnter');


const inpEnter = lblEnter.getElementsByTagName('input')[0];


const iEnter = lblEnter.getElementsByTagName('i')[0];


const lblConfirm = document.getElementById('lblConfirm');

const inpConfirm = lblConfirm.getElementsByTagName('input')[0];

const iConfirm = lblConfirm.getElementsByTagName('i')[0];



frmPass.addEventListener('submit',function (event) {
    fnCheckPassEnterConfirm(event);
});

let generateError = function (text) {
    let error = document.createElement('div');
    error.className = 'error';
    error.style.color = 'red';
    error.innerHTML = text;
    return error;
};

let error;

iEnter.addEventListener('click', function(){
    switch (this.className) {
        case 'fas fa-eye icon-password':
            this.className = 'fas fa-eye-slash icon-password';
            inpEnter.setAttribute('type','text');
            break;
        case 'fas fa-eye-slash icon-password':
            this.className = 'fas fa-eye icon-password';
            inpEnter.setAttribute('type','password');
            break;
    }
});

iConfirm.addEventListener('click', function(){
    switch (this.className) {
        case 'fas fa-eye icon-password':
            this.className = 'fas fa-eye-slash icon-password';
            inpConfirm.setAttribute('type','text');
            break;
        case 'fas fa-eye-slash icon-password':
            this.className = 'fas fa-eye icon-password';
            inpConfirm.setAttribute('type','password');
            break;
    }
});

function fnCheckPassEnterConfirm(event){

    if(inpEnter.value===''){
        alert('Нужно ввести пароль');
        event.preventDefault();
    }
    else if(inpConfirm.value===''){
        alert('Нужно подтвердить пароль');
        event.preventDefault();
    }
    else if(inpEnter.value===inpConfirm.value){

        if(error!==undefined){
            error.remove();
            error=undefined;
        }

        alert('You are welcome');
    }else{
                if(error===undefined) {
            error = generateError('Нужно ввести одинаковые значения');
            lblConfirm.append(error);
            event.preventDefault();
        }
    }}
