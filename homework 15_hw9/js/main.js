console.log('Homewwork 9_js');

let olltabs = document.querySelectorAll('li.tabs-title');
let contents = document.querySelectorAll('.tabs-content li');
let alltabls = document.getElementById('oMenu');

alltabls.onclick = function(event) {
    let target = event.target;

    if (target.tagName != 'LI') return;

    let i;

    let oActiveTab = document.querySelectorAll('li.tabs-title.active')[0];
    let oActiveContent = document.querySelectorAll('li.tabs-content.active')[0];

    oActiveTab.setAttribute('class','tabs-title');
    target.setAttribute('class','tabs-title active');

    i = -1;
    for (let oTab of olltabs) {
        i++;
        if(oTab.innerText===target.innerText){break}
    }


    let oContent = contents[i];

    if (oActiveContent.innerText != oContent.innerText){
        oContent.setAttribute('class','tabs-content active');
        oActiveContent.removeAttribute('class');
    }
};
