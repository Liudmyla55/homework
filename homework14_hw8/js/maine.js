/*
Опишите своими словами, как Вы понимаете, что такое обработчик событий.
Обработчик событий, это функция которая срабатывет когда событие произошло и
 благодаря обработчикам JavaScript-код может реагировать на действия посетителя.

 */

console.log('Homework_8.js');

let form = document.getElementById('formWithValidation');

let priceTextField =document.getElementById('price');

function fnCreateSpanPrice(oTextField,strSpanText){

    let oneDiv = document.createElement('div');
    let oneSpan = document.createElement('span');
    let butt = document.createElement('button');

    oneDiv.className='pane';

    oneSpan.className = 'strPrice';
    oneSpan.innerText = strSpanText;
    oneDiv.appendChild(oneSpan);

    butt.className = 'remove-button';
    butt.innerHTML='&times;';

    butt.onclick = function(){
        let oParent = this.parentNode;
        //При нажатии на Х - span с текстом и кнопка X должны быть удалены.
        oParent.remove();
        // Значение, введенное в поле ввода, обнуляется.
        oTextField.value = '';
    };

    oneDiv.appendChild(butt);

    return oneDiv;
}

let generateError = function (text) {
    let error = document.createElement('div');
    error.className = 'error';
    error.style.color = 'red';
    error.innerHTML = text;
    return error;
};

let onePane;
let error;

price.addEventListener('focus', function(){
        this.style.border = '2px solid green';
    if(error!=undefined){
        error.remove();
    }
    if(onePane!=undefined){
        onePane.remove();
    }
});

price.addEventListener('blur', function(){
        this.style.border = '1px solid grey';
    if(this.value===''){
    }
    else if (this.value >= 0) {
        if (form.querySelector('.pane') === null) {
            onePane = fnCreateSpanPrice(this, `Теущая цена: ${this.value}`);
            this.parentElement.insertBefore(onePane, this);
        } else {
            onePane.querySelector('.strPrice').innerText = `Текущая цена: ${this.value}`;
        }
    }
    else if (this.value < 0){
              this.style.border = '2px solid red';
                error = generateError('Please enter correct price.');
        this.parentElement.append(error);

    }
    else{
        this.style.border = '2px solid red';
        error = generateError('Please enter correct price.');
        this.parentElement.append(error);
    }

});
