'use strict';
let reseptionMap = new Map([
    ['огурцы', '500 гр'],
    ['помидоры', '300 гр'],
    ['сметана', '100 гр']
]);
//цикл по ключам
for (let fruit of reseptionMap.keys()) {
    alert(fruit);
}
//цикл по значению
for (let amount of reseptionMap.values()) {
    alert(amount);
}
//цикл по записям
    for(let entry of reseptionMap){
    alert(entry);
}

// reseptionMap.forEach((value, key, map)=>{
//         alert('$(key); $(value)');
// });

reseptionMap.forEach( (value, key, map) => {
    alert(`${key}: ${value}`); // огурцов: 500 гр, и т.д.
});