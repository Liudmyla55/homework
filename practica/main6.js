let cats=['Strelka','Belka','Murka'];
console.log(cats.length);
cats.length=2;
console.log(cats);

let coloor=['red', 'green', 'blue'];

//первый способ перебора элементов

// for (let i=0; i<coloor.length; i++){
//     console.log(coloor[i]);
// }

// //иторой способ перебора элементов
// coloor.forEach(function (coloor) {
//     console.log(coloor);
// });

//третий вариант:стрелочный вариант
coloor.forEach(coloor => console.log(coloor));

let myarr=['A','D','C'];
myarr=myarr.concat('one','two','three');
console.log(myarr);

let dog=['Жучка',  'Белка', 'Стрелка'];
mydog=dog.join('+');
console.log(mydog);

let dogs=['Жучка',  'Белка', 'Стрелка'];
dogs.push('Рыжик');
console.log(dogs);
dogs.pop();
console.log(dogs);
dogs.shift();
console.log(dogs);
dogs.unshift('Симка');
console.log(dogs);

let a = new Array(4);
for (i = 0; i < 4; i++) {
    a[i] = new Array(4);
    for (j = 0; j < 4; j++) {
        a[i][j] = "[" + i + "," + j + "]";
    }
    console.log(a);
}

let number=['one','two', 'three'];
console.log(number);
number[3]='four';
console.log(number);
number[1]='five';
console.log(number);