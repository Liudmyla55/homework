'use strict';
let set= new Set();
let vasya={name: 'Вася'};
let sergey={name: 'Сергей'};
let  maria={name: 'Мария'};


// посещения, некоторые пользователи заходят несколько раз
set.add(vasya);
set.add(sergey);
set.add(maria);
set.add(maria);
set.add(vasya);

// set сохраняет только уникальные значения
alert(set.size);

set.forEach(user => alert(user.name));


